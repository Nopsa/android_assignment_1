package com.example.downloadimage;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private String mChosenColor;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		SharedPreferences settings = getSharedPreferences("MainPrefs", 0);
		mChosenColor = settings.getString("Color", "");

		if (mChosenColor.length() > 0) {
			TextView text = (TextView) findViewById(R.id.fav_color_text_view);
			text.setText("The color you chose was: " + mChosenColor);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		SharedPreferences settings = getSharedPreferences("MainPrefs", 0);
		SharedPreferences.Editor editor = settings.edit();

		if (mChosenColor.length() > 0)
			editor.putString("Color", mChosenColor);

		editor.commit();
	}

	public void startGetUrlActivity(View view) {
		Intent intent = new Intent(MainActivity.this, GetUrlActivity.class);
		startActivity(intent);
	}

	public void startLoadDbActivity(View view) {
		Intent intent = new Intent(MainActivity.this, LoadDbActivity.class);
		startActivity(intent);
	}

	public void setCyan(View view) {
		mChosenColor = "Cyan";

		TextView text = (TextView) findViewById(R.id.fav_color_text_view);
		text.setText("You like the color " + mChosenColor + " best");
	}

	public void setOrange(View view) {
		mChosenColor = "Orange";

		TextView text = (TextView) findViewById(R.id.fav_color_text_view);
		text.setText("You like the color " + mChosenColor + " best");
	}

	public void deleteDb(View view) {

		Context context = getApplicationContext();
		CharSequence text = "";
		int duration = Toast.LENGTH_SHORT;

		final String SAMPLE_DB_NAME = "images.db";
		final String SAMPLE_TABLE_NAME = "Images";

		SQLiteDatabase sampleDB = null;

		sampleDB = this
				.openOrCreateDatabase(SAMPLE_DB_NAME, MODE_PRIVATE, null);

		if (sampleDB != null) {
			sampleDB.execSQL("DELETE FROM " + SAMPLE_TABLE_NAME);
		}

		sampleDB.close();

		text = "Database deleted";
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
}
