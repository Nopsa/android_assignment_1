package com.example.downloadimage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.GridView;
import android.widget.Toast;

public class LoadDbActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load_db);

		List<String> imgUrl = loadDb();

		if (imgUrl.size() > 0) {
			new GetImagesTask().execute(loadDb());
		} else {
			Context context = getApplicationContext();
			CharSequence text = "Database is empty!";
			int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast.makeText(context, text, duration);
			toast.show();
		}
	}

	private class GetImagesTask extends AsyncTask<List<String>, String, Void> {
		private ProgressDialog mDialog;
		private ImageAdapter mAdapter;
		private GridView mGridView;

		@Override
		protected Void doInBackground(List<String>... URLS) {
			int i = 0;

			for (String imgurl : URLS[0]) {
				i += 1;
				publishProgress("Downloading image " + i + " of "
						+ URLS[0].size());
				mAdapter.add(DownloadImage(imgurl));
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			this.mDialog.setCancelable(false);
			this.mDialog.setMessage(values[0]);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			mAdapter = new ImageAdapter(getBaseContext());
			mGridView = (GridView) findViewById(R.id.grid_view);

			this.mDialog = new ProgressDialog(LoadDbActivity.this);
			this.mDialog.setMessage("Please wait");
			this.mDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (this.mDialog.isShowing()) {
				this.mDialog.dismiss();
			}

			mGridView.setAdapter(mAdapter);
		}

	}

	/**
	 * Downloads a Bitmap from given url.
	 * 
	 * @param URL the url to download the Bitmap from.
	 * @return the Bitmap downloaded.
	 */
	private Bitmap DownloadImage(final String URL) {
		Bitmap bitmap = null;
		InputStream in = null;
		try {
			in = openHttpConnection(URL);
			bitmap = BitmapFactory.decodeStream(in);
			in.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return bitmap;
	}

	/**
	 * Opens a connection to requested url if possible.
	 * 
	 * @param urlString the url to connect to.
	 * @return the input stream for the requested site.
	 * @throws IOException
	 */
	private InputStream openHttpConnection(final String urlString)
			throws IOException {
		InputStream in = null;
		int response = -1;
		URL url;

		try {
			url = new URL(urlString);
		} catch (MalformedURLException e1) {
			throw new IOException("Could not create an url");
		}

		final URLConnection connection = url.openConnection();

		if (!(connection instanceof HttpURLConnection)) {
			throw new IOException("Not an http connection");
		}

		try {
			final HttpURLConnection httpConnection = (HttpURLConnection) connection;

			httpConnection.setReadTimeout(20000);
			httpConnection.setConnectTimeout(15000);
			httpConnection.setRequestMethod("GET");
			httpConnection.setDoInput(true);

			connection.connect();
			response = httpConnection.getResponseCode();

			if (response == HttpURLConnection.HTTP_OK) {
				in = httpConnection.getInputStream();
			}
		} catch (Exception ex) {
			throw new IOException("Could not create a connection");
		}
		return in;
	}

	/**
	 * Connects to our SQLite database and
	 * loads image url's from.
	 * 
	 * @return all url's loaded from the database.
	 */
	private List<String> loadDb() {
		List<String> imgUrl = new ArrayList<String>();

		final DatabaseHelper dbHelper = new DatabaseHelper(getBaseContext());
		final SQLiteDatabase db = dbHelper.getWritableDatabase();
		final Cursor c = db.query("Images", new String[] { "imgUrl" }, null,
				null, null, null, null);

		c.moveToFirst();
		while (!c.isAfterLast()) {
			imgUrl.add(c.getString(c.getColumnIndex("imgUrl")));
			c.moveToNext();
		}
		c.close();
		return imgUrl;
	}
}
