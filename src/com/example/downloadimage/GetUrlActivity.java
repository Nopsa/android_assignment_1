package com.example.downloadimage;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class GetUrlActivity extends Activity {
	private String mUrlPath;
	private EditText mText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_url);
		
		this.mUrlPath = "";
		this.mText = (EditText) findViewById(R.id.editTextField);
		this.mText.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
				if (arg1 == EditorInfo.IME_ACTION_GO) {
					startDownloadImageActivity();
				}
				return false;
			}
		});
	}
	
	/**
	 * Starts the download activity and give it the url to open.
	 */
	private void startDownloadImageActivity() {
		Intent intent = new Intent(GetUrlActivity.this, ImageActivity.class);

		/* Get the text from the text field and make it into a string. */
		this.mUrlPath = this.mText.getText().toString();
		
		/** 
		 * Check if the text given contains "http://" at the start, 
		 * then add it if it's not.
		 *
		 * TODO Add "http://" as default start in the editTextField. 
		 */
		if (this.mUrlPath.startsWith("http://") == false) {
			this.mUrlPath = "http://" + this.mUrlPath;
		}

		intent.putExtra("url", this.mUrlPath);

		startActivity(intent);
	}

}
