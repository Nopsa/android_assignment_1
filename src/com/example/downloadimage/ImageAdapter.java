package com.example.downloadimage;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
	private Context mContext;
	private List<Bitmap> mImage = new ArrayList<Bitmap>();

	public void add(Bitmap image) {
		mImage.add(image);
	}

	// Constructor
	public ImageAdapter(Context c) {
		mContext = c;
	}
	
	public List<Bitmap> getAll()
	{	
		return mImage;
	}

	@Override
	public int getCount() {
		return mImage.size();
	}

	@Override
	public Object getItem(int position) {
		return mImage.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(mContext);
		View gridView = new View(mContext);
		gridView = inflater.inflate(R.layout.per_image, parent,  false);
		
		ImageView imageView = (ImageView) gridView.findViewById(R.id.grid_item_image);
		imageView.setImageBitmap(mImage.get(position));
		return imageView;
	}

}