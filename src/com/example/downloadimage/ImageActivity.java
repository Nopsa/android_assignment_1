package com.example.downloadimage;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

public class ImageActivity extends Activity {
	List<String> mImageUrl = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image);

		final String url = getIntent().getExtras().getString("url");
		new DownloadImageTask().execute(url);
	}

	private class DownloadImageTask extends AsyncTask<String, String, Void> {
		private ImageAdapter mAdapter;
		private ProgressDialog mDialog;
		private GridView mGridView;

		private String getSiteContent(InputStream siteStream) {
			char[] inputBuffer = new char[2000];
			InputStreamReader isr = new InputStreamReader(siteStream);

			int charRead;
			String returnString = "";

			try {
				while ((charRead = isr.read(inputBuffer)) > 0) {
					String readString = String.copyValueOf(inputBuffer, 0,
							charRead);
					returnString += readString;
					inputBuffer = new char[2000];
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return returnString;
		}

		@Override
		protected Void doInBackground(final String... pageUrl) {
			InputStream siteStream = null;
			String siteContent = "";
			List<String> imgTag = new ArrayList<String>();
			List<String> imgUrlPath = new ArrayList<String>();

			try {
				publishProgress("Connecting...");
				siteStream = openHttpConnection(pageUrl[0]);
				publishProgress("Getting Site content");
				siteContent = getSiteContent(siteStream);
				siteStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			publishProgress("Getting image urls...");
			imgTag = getImageTag(siteContent);
			imgUrlPath = getImageByHttp(imgTag);
			mImageUrl = imgUrlPath;

			int i = 0;

			for (String imgUrl : imgUrlPath) {
				i += 1;
				publishProgress("Downloading image " + i + " of "
						+ imgUrlPath.size());
				mAdapter.add(DownloadImage(imgUrl));
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(final String... values) {
			super.onProgressUpdate(values);
			this.mDialog.setCancelable(false);
			this.mDialog.setMessage(values[0]);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.mAdapter = new ImageAdapter(ImageActivity.this);
			this.mDialog = new ProgressDialog(ImageActivity.this);
			this.mGridView = (GridView) findViewById(R.id.grid_view);

			this.mDialog.setMessage("Please wait");
			this.mDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			Button button = (Button) findViewById(R.id.save_button);
			button.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {

					save();
				}
			});

			this.mGridView.setAdapter(this.mAdapter);

			if (this.mDialog.isShowing()) {
				this.mDialog.dismiss();
			}
		}

		/**
		 * Save all image url's to the database.
		 */
		public void save() {
			final DatabaseHelper dbHelper = new DatabaseHelper(getBaseContext());
			final SQLiteDatabase db = dbHelper.getReadableDatabase();
			final ContentValues values = new ContentValues();

			for (String str : mImageUrl) {
				values.put("imgUrl", str);
				db.insert("Images", null, values);
			}

			Context context = getApplicationContext();
			CharSequence text = "Images saved to database";
			int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast.makeText(context, text, duration);
			toast.show();

			db.close();
		}
	}
	
	/**
	 * Finds all image urls from a string.
	 * 
	 * @param webContent contains the whole site in a string.
	 * @return a list of all image url's
	 */
	private List<String> getImageTag(final String webContent) {
		Document doc = Jsoup.parse(webContent);
		List<String> imgList = new ArrayList<String>();

		Elements pngs = doc.select("img[src~=(?i)\\.(png|jpe?g|gif)]");

		for (Element png : pngs) {
			imgList.add(png.attr("src"));
		}
		return imgList;
	}

	/**
	 * Downloads an image from requested url.
	 * 
	 * @param URL the url to get the image from.
	 * @return the bitmap downloaded from the url.
	 */
	private Bitmap DownloadImage(final String URL) {
		Bitmap bitmap = null;
		InputStream in = null;
		try {
			in = openHttpConnection(URL);
			bitmap = BitmapFactory.decodeStream(in);
			in.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return bitmap;
	}
	
	/**
	 * Checks if the image can be reached through http://
	 * 
	 * @param imgTag all image links to be checked.
	 * @return all images that can be reached through http://
	 */
	private List<String> getImageByHttp(List<String> imgTag) {
		List<String> imgUrlPath = new ArrayList<String>();

		for (String imgUrl : imgTag) {
			if (imgUrl.startsWith("http://")) {
				imgUrlPath.add(imgUrl);
			}
		}
		return imgUrlPath;
	}

	/**
	 * Creates an input stream to the requested site.
	 * 
	 * @param urlString
	 * @return
	 * @throws IOException
	 */
	private InputStream openHttpConnection(final String urlString)
			throws IOException {
		InputStream in = null;
		int response = -1;
		URL url;

		try {
			url = new URL(urlString);
		} catch (MalformedURLException e1) {
			throw new IOException("Could not create an url");
		}

		// Open a connection.
		final URLConnection connection = url.openConnection();

		// Check if it is a http connection.
		if (!(connection instanceof HttpURLConnection)) {
			throw new IOException("Not an http connection");
		}

		try {
			final HttpURLConnection httpConnection = (HttpURLConnection) connection;

			httpConnection.setReadTimeout(20000);
			httpConnection.setConnectTimeout(15000);
			httpConnection.setRequestMethod("GET");
			httpConnection.setDoInput(true);

			connection.connect();
			response = httpConnection.getResponseCode();

			if (response == HttpURLConnection.HTTP_OK) {
				in = httpConnection.getInputStream();
			}
		} catch (Exception ex) {
			throw new IOException("Could not create a connection");
		}
		return in;
	}
}
